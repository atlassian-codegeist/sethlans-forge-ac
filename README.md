# SETHLANS #



### What is SETHLANS? ###

**SETHLANS (Structured Ecosystem and Technical Hierarchy of Linked Atlassian Novel Solutions)** 
is a collection of **Atlassian** cloud apps utilizing the **Forge** development platform.

These apps include:

* **Jira-Forge-On-Helium** is an integration of **Jira** and the **Helium Network**.

* **Confluence-Forge-On-Helium** is an integration of **Confluence** and the **Helium Network**.

* **PyAtlasForgeSDK** is a software development kit for python and the **Atlassian Forge** platform.

* **ForgeSquarePOS** is an integration of the **Atlassian Forge** platform and **Square** small business APIs.